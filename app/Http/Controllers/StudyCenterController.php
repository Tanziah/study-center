<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StudyCenter;
use App\Models\Disttrict;
use App\Models\Upazilla;
use App\Models\School;
use App\Models\Program;

Use Validator,DB;
class StudyCenterController extends BaseController
{
   

    public function studyCenter(){
        $rcList = Disttrict::get();
        $schoolList = School::get();
    	return view('study_center', compact(
            'rcList','schoolList' 
        ));
    
    }

    public function storeStudyCenter(Request $request)
    {
    	//dd($request->all());
    	$validator= Validator::make($request->all(),[
            'rc'                    =>'required',
            'src'                   =>'required',
            'pgrm_name'             =>'required',
    		    'center_name'           =>'required',
            'center_name_bangla'    =>'required',
            'std_center_code'       =>'required',
    		    'address'               =>'required',
            'status'                =>'required'
    	]);


    	if($validator->fails()){
    		return back()
    			->withErrors($validator)
    			->withInput();
    	}
    	else
        {
	     $center= new StudyCenter;
        $center->ak_dis_id               = $request->rc;
        $center->akup_id                 = $request->src;
        $center->ak_schl_id              = $request->scl_name;
        $center->ak_prgm_id              = $request->pgrm_name;
	      $center->ak_study_center         = $request->center_name;
        $center->ak_study_center_bangla  = $request->center_name_bangla;
        $center->ak_study_center_code    = $request->std_center_code;
	      $center->ak_address	             = $request->address;
        $center->ak_status               = $request->status;
    

    		if ($center->save())
            {
                return back()
                    ->withInput()
                    ->with('success', 'Successfuly Saved.');
            }   
            else
            {
                return back()                    
                    ->withInput()->with('error', 'Please try again.');
            }
    	}
    }
  # Return SRC  List by RC
    public function getSrc(Request $request)
    {
        $list = "<option value=\"\">Select SRC</option>";
        if (!empty($request->rc_id))
        { 

            $desList  = Upazilla::where('ak_dis_id', $request->rc_id)
                       ->get(); 

            foreach ($desList as  $value) 
            {
                $list .= "<option value=\"$value->up_id\">$value->upzilla_name</option>";
            }
        }
        return $list;
    }  
  # Return Program  List by School
    public function getProgram(Request $request)
    {
        $list = "<option value=\"\">Select Program</option>";
        if (!empty($request->sc_id))
        { 

            $desList  = Program::where('ak_schl_id', $request->sc_id)
                       ->get(); 

            foreach ($desList as  $value) 
            {
                $list .= "<option value=\"$value->ak_prgm_id\">$value->ak_prgm_name</option>";
            }
        }
        return $list;
    }    

 # Center List Form   
    public function centerList(){
        $rcList = Disttrict::get();
        $schoolList = School::get();
        $scode= StudyCenter::get();
        $sclName= School::get();
        $programs= DB::table('ak_study_center as sc')
                    ->Select(
                        'sc.*' ,                    
                        'u.upzilla_name',
                        'p.ak_prgm_name',
                        'sl.ak_schl_name'
                    )
                 
                  ->leftJoin('upazilla AS u', 'u.up_id', '=', 'sc.akup_id')
                  ->leftJoin('ak_program_name AS p', 'p.ak_prgm_id', '=', 'sc.ak_prgm_id')
                  ->leftJoin('ak_school AS sl', 'sl.ak_schl_id', '=', 'p.ak_schl_id')
                  ->paginate(10);
        return view('center_list', compact(
            'rcList','schoolList','programs','scode'
        ));
    
    }
    public function getCenterList(Request $request){
     // dd($request->all());
        $rc_id =$request->rc_id;
        $src   =$request->src;
        $scode =$request->scode;
        $scl   =$request->scl;
        $prg   =$request->prg;

        
        $programs1= DB::table('ak_study_center as sc')
                    ->Select(
                        'sc.*' ,                    
                        'u.upzilla_name',
                        'p.ak_prgm_name',
                        'sl.ak_schl_name'
                    )
                  ->leftJoin('k_disttrict AS d', 'd.ak_dis_id', '=', 'sc.ak_dis_id')
                  ->leftJoin('upazilla AS u', 'u.up_id', '=', 'sc.akup_id')
                  ->leftJoin('ak_program_name AS p', 'p.ak_prgm_id', '=', 'sc.ak_prgm_id')
                  ->leftJoin('ak_school AS sl', 'sl.ak_schl_id', '=', 'p.ak_schl_id')
                  ->where(function($condition) use ($rc_id,$src,$scode,$scl,$prg){
                      if (!empty($rc_id)) 
                      {
                        $condition->where("sc.ak_dis_id", $rc_id);
                      }
                      if (!empty($src)) 
                      {
                        $condition->where("sc.akup_id", $src);
                      }
                      if (!empty($scode)) 
                      {
                        $condition->where("sc.ak_study_center_code", $scode);
                      }
                      if (!empty($scl)) 
                      {
                        $condition->where("sc.ak_schl_id", $scl);
                      }
                      if (!empty($prg)) 
                      {
                        $condition->where("sc.ak_prgm_id", $prg);
                      }
                    });
                //->where("sc.akup_id", $src);
                 // ->paginate(10);

        $programs=$programs1->paginate(10);
         $programs2=$programs1->first();
            

            $list = "";

             if($programs2==""){
                $list = "<tr><td colspan='7' align='center'><h4><font  color='red'>No Center Found!</font></h4></td></tr>";
                } 
               else{  
      
            foreach ($programs as  $prog) 
            {
                $url_edit=url('/centeredit/'.$prog->ak_centr_id);
                $url_delet=url('/centerdelete/'.$prog->ak_centr_id);

                if($prog->ak_status==1)
                 { $status='<label class="col-sm-12 badge badge-success">Active</label>';
                 }
                 else
                 { $status='<label class="col-sm-12 badge badge-danger">Inactive</label>';
                 }
              

                $list .= "<tr>
                          <td>$prog->ak_study_center</td>
                          <td>$prog->ak_study_center_code</td>
                          <td>$prog->upzilla_name</td>
                          <td with='200'>$prog->ak_prgm_name</td>
                          <td>$prog->ak_schl_name</td>
                          <td>
                            $status
                          </td>
                          <td> 
                            <a type=\"button\" href=\"$url_edit\" title=\"Update\"><i class=\"fa fa-pencil\" style=\"font-size:18px\"></i> </a> | 

                            <a href=\"$url_delet\" onclick=\"return confirm('Are you sure you want to delete this Center?');\" title=\"Delete\"><i class=\"ace-icon fa fa-trash bigger-120 danger\" style=\"font-size:18px;color:#E00201\"></i></a></td>
                        </tr>";
              }
            
             } 
           


        return $list;
     }

    public function school(){

        return view("school");
    }
    public function schoolStore(Request $request)
    {
        //dd($request->all());
        $validator= Validator::make($request->all(),[
            'school'    =>'required'
        ]);

        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            $center= new School;
            $center->ak_schl_name   = $request->school;

            if ($center->save())
            {
                return back()
                    ->withInput()
                    ->with('success', 'Successfuly Saved.');
            }   
            else
            {
                return back()                    
                    ->withInput()->with('error', 'Please try again.');
            }
        }
    } 

    public function program(){
        $schoolList = School::get();
        return view("program", compact(
            'schoolList' 
        ));
    
    }
    public function programStore(Request $request)
    {
        //dd($request->all());
        $validator= Validator::make($request->all(),[
            'school'            =>'required',
            'program'           =>'required'
        ]);


        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            $center= new Program;
            $center->ak_schl_id        = $request->school;
            $center->ak_prgm_name      = $request->program;
        

            if ($center->save())
            {
                return back()
                    ->withInput()
                    ->with('success', 'Successfuly Saved.');
            }   
            else
            {
                return back()                    
                    ->withInput()->with('error', 'Please try again.');
            }
        }
    }  

    public function centerEdit($id){
        $rcList = Disttrict::get();
        $schoolList = School::get();
        $stdcenter = DB::table('ak_study_center as sc')
                    ->Select(
                        'sc.*' ,                    
                        'u.upzilla_name',
                        'u.up_id',
                        'd.dis_name',
                        'd.ak_dis_id',
                        'p.ak_prgm_name',
                        'p.ak_prgm_id',
                        'sl.ak_schl_name',
                        'sl.ak_schl_id'
                    )
                 
                  ->leftJoin('upazilla AS u', 'u.up_id', '=', 'sc.akup_id')
                  ->leftJoin('k_disttrict AS d', 'd.ak_dis_id', '=', 'u.ak_dis_id')
                  ->leftJoin('ak_program_name AS p', 'p.ak_prgm_id', '=', 'sc.ak_prgm_id')
                  ->leftJoin('ak_school AS sl', 'sl.ak_schl_id', '=', 'p.ak_schl_id')
                  ->where('sc.ak_centr_id', $id)
                  ->first();
        return view('study_center_edit', compact(
            'rcList','schoolList','stdcenter' 
        ));
    
    } 
   public function programUpdate(Request $request)
    {
 
        $validator= Validator::make($request->all(),[
            'src'                   =>'required',
            'pgrm_name'             =>'required',
            'center_name'           =>'required',
            'center_name_bangla'    =>'required',
            'std_center_code'       =>'required',
            'address'               =>'required'
        ]);


        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            $center = StudyCenter::where('ak_centr_id', $request->std_id)->update([
               'ak_dis_id' => $request->rc,
               'akup_id' => $request->src,
               'ak_schl_id' => $request->scl_name,
               'ak_prgm_id' => $request->pgrm_name,
               'ak_study_center' => $request->center_name,
               'ak_study_center_bangla' => $request->center_name_bangla,
               'ak_study_center_code' => $request->std_center_code,
               'ak_address' => $request->address
        ]);


                return back()
                    ->withInput()
                    ->with('success', 'Successfuly Updated.');
             
            
        }
    }

    public function centerDelete($id){
       
        StudyCenter::where('ak_centr_id', $id)->delete();
       
        return back()
        ->with('success', "Buyer Deleted Successfully!!");
    }
}
