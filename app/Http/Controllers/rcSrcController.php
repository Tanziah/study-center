<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StudyCenter;
use App\Models\Disttrict;
use App\Models\Upazilla;

Use Validator,DB;
class rcSrcController extends BaseController
{
    public function src(){
        $rcList = Disttrict::get();
        return view("src", compact(
            'rcList' 
        ));
    
    }
    public function storeSrc(Request $request)
    {
        //dd($request->all());
        $validator= Validator::make($request->all(),[
            'src'                   =>'required',
            'rc'                    =>'required'
        ]);


        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            $center= new Upazilla;
            $center->ak_dis_id         = $request->rc;
            $center->upzilla_name      = $request->src;
        

            if ($center->save())
            {
                return back()
                    ->withInput()
                    ->with('success', 'Successfuly Saved.');
            }   
            else
            {
                return back()                    
                    ->withInput()->with('error', 'Please try again.');
            }
        }
    }


}
