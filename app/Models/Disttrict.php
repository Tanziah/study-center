<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Disttrict extends Model
{
    protected $table=  'k_disttrict';
    
    public $timestamps= false;
}
