<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table=  'ak_program_name';
    
    public $timestamps= false;
}
