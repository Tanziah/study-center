<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table=  'ak_school';
    
    public $timestamps= false;
}
