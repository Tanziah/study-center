<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudyCenter extends Model
{
    protected $table=  'ak_study_center';
    
    public $timestamps= false;
}
