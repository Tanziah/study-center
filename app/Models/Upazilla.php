<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Upazilla extends Model
{
    protected $table=  'upazilla';
    
    public $timestamps= false;
}
