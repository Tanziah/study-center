-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 26, 2019 at 12:18 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ktj_study_center_ak`
--

-- --------------------------------------------------------

--
-- Table structure for table `ak_program_name`
--

CREATE TABLE `ak_program_name` (
  `ak_prgm_id` int(11) NOT NULL,
  `ak_schl_id` int(11) NOT NULL,
  `ak_prgm_name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ak_program_name`
--

INSERT INTO `ak_program_name` (`ak_prgm_id`, `ak_schl_id`, `ak_prgm_name`) VALUES
(2, 2, 'Secondary School Certificate (SSC)'),
(3, 2, 'Higher Secondary Certificate (HSC) and'),
(4, 2, 'Bachelor of Business Studies (BBS)'),
(5, 3, 'Bachelor of Agricultural Education (B. Ag. Ed)'),
(6, 3, 'Diploma in Youth in Development Work (DYDW) 3. Certificate in Livestock & Poultry (CLP)'),
(7, 3, 'Certificate in Pisciculture & Fish Processing (CPFP)  5. Master of Science (MS) in Agricultural Sciences'),
(8, 4, 'Bachelor of Business Administration (BBA) Program'),
(9, 4, 'Post-Graduate Diploma in Management (PGDM) Program'),
(10, 4, 'Master of Business Administration (MBA) Program'),
(11, 4, 'Commonwealth Executive MBA/MPA Program'),
(12, 4, 'Master of Business Administration (Evening) Program'),
(13, 4, 'Master of Philosophy (MPhil) Program'),
(14, 4, 'Doctor of Philosophy (Ph.D) Program'),
(15, 5, 'Master of Education (MEd)'),
(16, 5, 'Bachelor of Education (BEd)'),
(17, 5, 'Certificate in Education (CEd)'),
(18, 6, 'Bachelor of Arts (BA)/Bachelor of Social Science (BSS)'),
(19, 6, 'Bachelor of English Language Teaching (BELT)'),
(20, 6, 'Certificate in Arabic Language Proficiency (CALP)'),
(21, 6, 'Certificate in English Language Proficiency (CELP)'),
(22, 7, 'Diploma in Computer Science and Application( DCSA)'),
(23, 7, 'B.Sc.-in-Nursing (BSN) Program'),
(24, 7, 'Bachelor of Science in Computer Science and Engineering(cse)');

-- --------------------------------------------------------

--
-- Table structure for table `ak_school`
--

CREATE TABLE `ak_school` (
  `ak_schl_id` int(11) NOT NULL,
  `ak_schl_name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ak_school`
--

INSERT INTO `ak_school` (`ak_schl_id`, `ak_schl_name`) VALUES
(2, 'Open School'),
(3, 'School of Agriculture & Rural Development (SARD)'),
(4, 'School of Business (SoB)'),
(5, 'School of Education (SoE)'),
(6, 'School of Social Science, Humanities & Languages (SSHL)'),
(7, 'School of Science & Technology (SST)');

-- --------------------------------------------------------

--
-- Table structure for table `ak_study_center`
--

CREATE TABLE `ak_study_center` (
  `ak_centr_id` int(11) NOT NULL,
  `ak_dis_id` int(11) NOT NULL,
  `akup_id` int(11) NOT NULL,
  `ak_schl_id` int(11) NOT NULL,
  `ak_prgm_id` int(11) NOT NULL,
  `ak_study_center` varchar(150) CHARACTER SET utf8 NOT NULL,
  `ak_study_center_bangla` varchar(150) CHARACTER SET utf8 NOT NULL,
  `ak_study_center_code` varchar(150) CHARACTER SET utf8 NOT NULL,
  `ak_address` varchar(150) CHARACTER SET utf8 NOT NULL,
  `ak_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ak_study_center`
--

INSERT INTO `ak_study_center` (`ak_centr_id`, `ak_dis_id`, `akup_id`, `ak_schl_id`, `ak_prgm_id`, `ak_study_center`, `ak_study_center_bangla`, `ak_study_center_code`, `ak_address`, `ak_status`) VALUES
(1, 1, 1, 3, 6, 'test center', 'বাংলা', '1234', 'testtt testtt', 1),
(2, 2, 7, 3, 5, 'Test Center 2', 'বাংলা', '4567', 'tttttt', 1),
(3, 10, 63, 4, 10, 'Test 3', 'আমরা', '3455', 'testt', 1),
(4, 1, 1, 2, 2, 'test center3', 'bbb', '5678', 'sdfsd', 1),
(5, 10, 64, 3, 5, 'test', 'এখন', '25369', 'ffdfgdfgd', 0);

-- --------------------------------------------------------

--
-- Table structure for table `k_disttrict`
--

CREATE TABLE `k_disttrict` (
  `ak_dis_id` int(4) NOT NULL,
  `dis_name` varchar(64) NOT NULL,
  `dis_name_bn` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `k_disttrict`
--

INSERT INTO `k_disttrict` (`ak_dis_id`, `dis_name`, `dis_name_bn`) VALUES
(1, 'Dhaka', 'ঢাকা'),
(2, 'Faridpur', 'ফরিদপুর'),
(10, 'Mymensingh', 'ময়মনসিংহ'),
(18, 'Bogra', 'বগুড়া'),
(24, 'Rajshahi', 'রাজশাহী'),
(32, 'Rangpur', 'রংপুর'),
(35, 'Barishal', 'বরিশাল'),
(43, 'Chittagong', 'চট্টগ্রাম'),
(44, 'Comilla', 'কুমিল্লা'),
(54, 'Sylhet', 'সিলেট'),
(57, 'Jessore', 'যশোর'),
(59, 'Khulna', 'খুলনা');

-- --------------------------------------------------------

--
-- Table structure for table `upazilla`
--

CREATE TABLE `upazilla` (
  `up_id` int(11) NOT NULL,
  `ak_dis_id` int(11) NOT NULL,
  `upzilla_name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `upazilla`
--

INSERT INTO `upazilla` (`up_id`, `ak_dis_id`, `upzilla_name`) VALUES
(1, 1, 'Dhamrai'),
(7, 2, 'Gopalganj'),
(8, 2, 'Madaripur'),
(9, 2, 'Rajbari'),
(10, 2, 'Faridpur'),
(14, 2, 'Shariatpur'),
(16, 3, 'Sreepur'),
(17, 3, 'Kapasia'),
(18, 3, 'Kaliganj'),
(19, 3, 'Kaliakoir'),
(21, 4, 'Muksudpur'),
(22, 4, 'Kotwalipara'),
(23, 4, 'Tungipara'),
(24, 4, 'Gopalganj-S'),
(25, 4, 'Kasiani'),
(26, 5, 'Dewanganj'),
(27, 5, 'Islampur'),
(28, 5, 'Jamalpur-S'),
(29, 5, 'Madarganj'),
(30, 5, 'Melendah'),
(31, 5, 'Sarishabari'),
(32, 5, 'Bakshiganj'),
(33, 6, 'Kishoreganj-S'),
(34, 6, 'Mithamoin'),
(35, 6, 'Tarail'),
(36, 6, 'Pakundia'),
(37, 6, 'Kuliarchar'),
(38, 6, 'Katiadi'),
(39, 6, 'Nikli'),
(40, 6, 'Karimganj'),
(41, 6, 'Austagram'),
(42, 6, 'Bajitpur'),
(43, 6, 'Bhairab'),
(44, 6, 'Hossainpur'),
(45, 6, 'Itna'),
(46, 7, 'Kalkini'),
(47, 7, 'Madaripur-S'),
(48, 7, 'Rajoir'),
(49, 7, 'Shibchar'),
(51, 8, 'Daulatpur'),
(52, 8, 'Harirampur'),
(53, 8, 'Saturia'),
(54, 8, 'Shivalaya'),
(55, 8, 'Singair'),
(56, 8, 'Ghior'),
(57, 9, 'Gazaria'),
(58, 9, 'Lauhajong'),
(59, 1, 'Munshigonj'),
(60, 1, 'Sreenagar'),
(61, 9, 'Tongibari'),
(62, 9, 'Sirajdikhan'),
(63, 10, 'Madhupur '),
(64, 10, 'Tangail '),
(70, 10, 'Mymensingh'),
(71, 10, 'Netrokona '),
(72, 10, 'Kishoreganj '),
(73, 10, 'Jamalpur'),
(74, 10, 'Sherpur'),
(75, 11, 'Araihazar'),
(76, 11, 'Rupganj'),
(77, 11, 'Bandar'),
(78, 11, 'Sonargaon'),
(79, 1, 'Narayangonj'),
(80, 12, 'Kendua'),
(81, 12, 'Atpara'),
(82, 12, 'Barhatta'),
(83, 12, 'Durgapur'),
(84, 12, 'Kalmakanda'),
(85, 12, 'Madan'),
(86, 12, 'Mohanganj'),
(87, 12, 'Netrakona-S'),
(88, 12, 'Purbadhala'),
(89, 12, 'Khaliajuri'),
(90, 13, 'Baliakandi'),
(91, 13, 'Goalanda'),
(92, 13, 'Pangsha'),
(93, 13, 'Rajbari-S'),
(94, 13, 'Kalukhali'),
(95, 14, 'Bhedarganj'),
(96, 14, 'Damuddya'),
(97, 14, 'Goshairhat'),
(98, 14, 'Naria'),
(99, 14, 'Shariatpur-S'),
(100, 14, 'Janjira'),
(101, 15, 'Nakla'),
(102, 15, 'Jhenaigati'),
(103, 15, 'Nalitabari'),
(104, 15, 'Sherpur-S'),
(105, 15, 'Sreebordi'),
(106, 16, 'Ghatail'),
(107, 16, 'Shakhipur'),
(108, 16, 'Mirzapur'),
(109, 16, 'Tangail-S'),
(110, 16, 'Madhupur'),
(111, 16, 'Kalihati'),
(112, 16, 'Gopalpur'),
(113, 16, 'Nagarpur'),
(114, 16, 'Bhuapur'),
(115, 16, 'Basail'),
(116, 16, 'Dhanbari'),
(117, 16, 'Delduar'),
(118, 17, 'Shibpur'),
(119, 17, 'Palash'),
(121, 17, 'Raipura'),
(122, 17, 'Belabo'),
(123, 17, 'Monohardi'),
(129, 18, 'Bogra'),
(130, 18, 'Sirajgonj'),
(131, 18, 'Gaibandha'),
(132, 18, 'Joypurhat'),
(133, 19, 'Tarash'),
(134, 19, 'Ullapara'),
(135, 19, 'Serajganj-S'),
(136, 19, 'Shahzadpur'),
(137, 19, 'Raiganj'),
(138, 19, 'Kazipur'),
(139, 19, 'Chowhali'),
(140, 19, 'Belkuchi'),
(141, 19, 'Kamarkhand'),
(142, 20, 'Adamdighi'),
(143, 20, 'Bogra-S'),
(144, 20, 'Dhunot'),
(145, 20, 'Dhupchancia'),
(146, 20, 'Gabtali'),
(147, 20, 'Kahaloo'),
(148, 20, 'Nandigram'),
(149, 20, 'Sariakandi'),
(150, 20, 'Sherpur'),
(151, 20, 'Shibganj'),
(152, 20, 'Sonatala'),
(153, 20, 'Shajahanpur'),
(154, 21, 'Joypurhat-S'),
(155, 21, 'Akkelpur'),
(156, 21, 'Khetlal'),
(157, 21, 'Panchbibi'),
(158, 21, 'Kalai'),
(159, 22, 'Mohadevpur'),
(160, 22, 'Shapahar'),
(161, 22, 'Raninagar'),
(162, 22, 'Porsha'),
(163, 22, 'Patnitala'),
(164, 22, 'Dhamoirhat'),
(165, 22, 'Naogaon-S'),
(166, 22, 'Niamatpur'),
(167, 22, 'Atrai'),
(168, 22, 'Manda'),
(169, 22, 'Badalgachi'),
(170, 23, 'Bagatipara'),
(171, 23, 'Baraigram'),
(172, 23, 'Gurudaspur'),
(173, 23, 'Lalpur'),
(174, 24, 'Natore'),
(175, 23, 'Singra'),
(176, 23, 'Naldanga'),
(180, 24, 'Noagaon'),
(182, 25, 'Atghoria'),
(183, 25, 'Bera'),
(184, 25, 'Chatmohar'),
(185, 25, 'Faridpur'),
(186, 25, 'Ishwardi'),
(187, 25, 'Bhangura'),
(188, 25, 'Sujanagar'),
(189, 25, 'Santhia'),
(190, 24, 'Pabna'),
(191, 26, 'Pirgacha'),
(192, 26, 'Rangpur-S'),
(193, 26, 'Pirganj'),
(194, 26, 'Mithapukur'),
(195, 26, 'Taraganj'),
(196, 26, 'Kaunia'),
(197, 26, 'Gangachara'),
(198, 26, 'Badarganj'),
(199, 27, 'Birol'),
(200, 27, 'Bochaganj'),
(201, 27, 'Chirirbandar'),
(202, 27, 'Fulbari'),
(203, 27, 'Ghoraghat'),
(204, 27, 'Hakimpur'),
(205, 27, 'Kaharol'),
(206, 27, 'Khanshama'),
(207, 27, 'Dinajpur-S'),
(208, 27, 'Nawabganj'),
(209, 27, 'Birampur'),
(210, 27, 'Parbatipur'),
(211, 27, 'Birganj'),
(212, 28, 'Saghata'),
(213, 28, 'Palashbari'),
(214, 28, 'Sundarganj'),
(215, 28, 'Sadullapur'),
(216, 28, 'Gobindaganj'),
(217, 28, 'Gaibandha-S'),
(218, 28, 'Fulchari'),
(219, 29, 'Bhurungamari'),
(220, 29, 'Rajibpur'),
(221, 29, 'Chilmari'),
(222, 29, 'Fulbari'),
(223, 29, 'Kurigram-S'),
(224, 29, 'Nageswari'),
(225, 29, 'Rowmari'),
(226, 29, 'Rajarhat'),
(227, 29, 'Ulipur'),
(228, 30, 'Patgram'),
(229, 30, 'Lalmonirhat-S'),
(230, 30, 'Kaliganj'),
(231, 30, 'Hatibandha'),
(232, 30, 'Aditmari'),
(233, 31, 'Dimla'),
(234, 31, 'Domar'),
(235, 31, 'Jaldhaka'),
(236, 31, 'Kishoreganj'),
(237, 31, 'Nilphamari-S'),
(238, 31, 'Sayedpur'),
(239, 32, 'Panchagarh'),
(240, 32, 'Kurigram'),
(241, 32, 'Nilphamari'),
(242, 32, 'Dinajpur'),
(243, 32, 'Lalmonirhat'),
(244, 33, 'Thakurgaon-S'),
(245, 33, 'Baliadangi'),
(246, 33, 'Haripur'),
(247, 33, 'Pirganj'),
(248, 33, 'Ranisankail'),
(249, 34, 'Banaripara'),
(250, 34, 'Agailjhara'),
(251, 34, 'Gouranadi'),
(252, 34, 'Hizla'),
(253, 34, 'Mehendiganj'),
(254, 34, 'Muladi'),
(255, 34, 'Patuakhali'),
(256, 34, 'Bakerganj'),
(257, 34, 'Uzirpur'),
(258, 34, 'Barisal-S'),
(260, 35, 'Bhola'),
(261, 35, 'Perojpur'),
(262, 35, 'Barguna'),
(263, 35, 'Barishal'),
(264, 35, 'Patuakhali'),
(265, 36, 'Bhola-S'),
(266, 36, 'Charfassion'),
(267, 36, 'Lalmohan'),
(268, 36, 'Monpura'),
(269, 36, 'Tazumuddin'),
(270, 36, 'Daulatkhan'),
(271, 36, 'Borhanuddin'),
(272, 37, 'Jhalokathi-S'),
(273, 37, 'Kathalia'),
(274, 37, 'Nalchity'),
(275, 37, 'Rajapur'),
(276, 38, 'Galachipa'),
(277, 38, 'Dashmina'),
(278, 38, 'Kalapara'),
(279, 38, 'Mirjaganj'),
(280, 38, 'Patuakhali-S'),
(281, 38, 'Dumki'),
(282, 38, 'Bauphal'),
(283, 38, 'Rangabali'),
(284, 38, 'Rangabali'),
(285, 39, 'Zianagar'),
(286, 39, 'Perojpur-S'),
(287, 39, 'Nazirpur'),
(288, 39, 'Nesarabad'),
(289, 39, 'Bhandaria'),
(290, 39, 'Kawkhali'),
(291, 39, 'Mothbaria'),
(292, 40, 'Sandwip'),
(293, 40, 'Lohagara'),
(294, 40, 'Satkania'),
(295, 40, 'Patiya'),
(296, 40, 'Chandanish'),
(297, 40, 'Boalkhali'),
(298, 40, 'Banskhali'),
(299, 40, 'Sitakunda'),
(300, 40, 'Raojan'),
(301, 40, 'Rangunia'),
(302, 40, 'Mirsharai'),
(303, 40, 'Hathazari'),
(304, 40, 'Fatikchari'),
(305, 40, 'Anwara'),
(306, 41, 'Bandarban-S'),
(307, 41, 'Rowangchari'),
(308, 41, 'Thanchi'),
(309, 41, 'Lama'),
(310, 41, 'Naikhyongchari'),
(311, 41, 'Alikadam'),
(312, 41, 'Ruma'),
(313, 42, 'Ashuganj'),
(314, 42, 'Nasirnagar'),
(315, 42, 'Sarail'),
(316, 42, 'Akhaura'),
(317, 42, 'Bancharampur'),
(318, 42, 'B.Baria-S'),
(319, 42, 'Kasba'),
(320, 42, 'Nabinagar'),
(321, 42, 'BijoyNagar'),
(322, 43, 'Cox’s Bazar'),
(323, 43, 'Rangamati'),
(324, 43, 'Bandarban'),
(325, 43, 'Chittagong'),
(326, 43, 'Khagrachhari'),
(331, 44, 'Daudkandi'),
(339, 44, 'Comilla'),
(341, 44, 'Chadpur'),
(342, 44, 'Laxmipur'),
(343, 44, 'Noakhali'),
(344, 44, 'Feni'),
(345, 44, 'Bbaria '),
(346, 45, 'Kutubdia'),
(347, 45, 'Cox\'s Bazar-S'),
(348, 45, 'Chakoria'),
(349, 45, 'Pekua'),
(350, 45, 'Moheskhali'),
(351, 45, 'Ramu'),
(352, 45, 'Ukhiya'),
(353, 45, 'Teknaf'),
(354, 46, 'Daganbhuiyan'),
(355, 46, 'Chhagalniya'),
(356, 46, 'Sonagazi'),
(357, 46, 'Feni-S'),
(358, 46, 'Porshuram'),
(359, 46, 'Fulgazi'),
(360, 47, 'Khagrachari-S'),
(361, 47, 'Mahalchari'),
(362, 47, 'Dighinala'),
(363, 47, 'Panchari'),
(364, 47, 'Ramgarh'),
(365, 47, 'Manikchari'),
(366, 47, 'Laxmichari'),
(367, 47, 'Matiranga'),
(368, 47, 'Guimara'),
(369, 48, 'Komol Nagar'),
(370, 48, 'Ramgati'),
(371, 48, 'Ramganj'),
(372, 48, 'Raipur'),
(373, 48, 'Laxmipur-S'),
(374, 49, 'Subarna Char'),
(375, 49, 'Kabir Hat'),
(376, 49, 'Sonaimuri'),
(377, 49, 'Noakhali-S'),
(378, 49, 'Chatkhil'),
(379, 49, 'Begumganj'),
(380, 49, 'Companiganj'),
(381, 49, 'Hatiya'),
(382, 49, 'Senbag'),
(383, 50, 'Rajosthali'),
(384, 50, 'Belaichari'),
(385, 50, 'Rangamati-S'),
(386, 50, 'Baghaichari'),
(387, 50, 'Kaptai'),
(388, 50, 'Nanniarchar'),
(389, 50, 'Juraichari'),
(390, 50, 'Langadu'),
(391, 50, 'Barkal'),
(392, 50, 'Kaukhali'),
(393, 51, 'Companiganj'),
(394, 51, 'Kanaighat'),
(395, 51, 'Dakshin Surma'),
(396, 51, 'Zakiganj'),
(397, 51, 'Sylhet-S'),
(398, 51, 'Jointiapur'),
(399, 51, 'Gowainghat'),
(400, 51, 'Fenchuganj'),
(401, 51, 'Biswanath'),
(402, 51, 'Beanibazar'),
(403, 51, 'Balaganj'),
(404, 51, 'Golapganj'),
(405, 51, 'Osmaninagar'),
(406, 52, 'Baniachong'),
(407, 52, 'Azmiriganj'),
(408, 52, 'Nabiganj'),
(409, 52, 'Madhabpur'),
(410, 52, 'Lakhai'),
(411, 52, 'Habiganj-S'),
(412, 52, 'Chunarughat'),
(413, 52, 'Bahubal'),
(414, 53, 'Kulaura'),
(415, 53, 'Kamalganj'),
(416, 53, 'Moulvibazar-S'),
(417, 53, 'Rajnagar'),
(418, 53, 'Sreemangal'),
(419, 53, 'Juri'),
(420, 53, 'Barlekha'),
(427, 54, 'Sylhet'),
(428, 54, 'Chhatak'),
(429, 54, 'Sunamganj'),
(430, 54, 'Moulvibazar'),
(431, 54, 'Habigonj'),
(432, 55, 'Dacope'),
(433, 55, 'Batiaghata'),
(434, 55, 'Dighalia'),
(435, 55, 'Dumuria'),
(436, 55, 'Koira'),
(437, 55, 'Paikgacha'),
(438, 55, 'Phultala'),
(439, 55, 'Terokhada'),
(440, 55, 'Rupsa'),
(441, 56, 'Mongla'),
(442, 56, 'Chitalmari'),
(443, 56, 'Sharankhola'),
(444, 56, 'Rampal'),
(445, 56, 'Mollahat'),
(446, 56, 'Kachua'),
(447, 56, 'Fakirhat'),
(448, 56, 'Bagerhat-S'),
(449, 56, 'Morrelganj'),
(450, 57, 'Chuadanga'),
(451, 57, 'Magura'),
(452, 57, 'Meherpur'),
(453, 57, 'Narail'),
(454, 58, 'Abhoynagar'),
(455, 58, 'Bagherpara'),
(456, 58, 'Chowgacha'),
(457, 58, 'Jhikargacha'),
(458, 58, 'Keshabpur'),
(459, 58, 'Jessore-S'),
(460, 58, 'Monirampur'),
(461, 58, 'Sarsha'),
(467, 59, 'Khulna                                                          '),
(468, 60, 'Khoksha'),
(469, 60, 'Bheramara'),
(470, 60, 'Daulatpur'),
(471, 60, 'Kumarkhali'),
(472, 60, 'Mirpur'),
(473, 60, 'Kushtia-S'),
(474, 61, 'Salikha'),
(475, 61, 'Magura-S'),
(476, 61, 'Mohammadpur'),
(477, 61, 'Sreepur'),
(478, 62, 'Gangni'),
(479, 62, 'Mujib Nagar'),
(480, 62, 'Meherpur-S'),
(481, 63, 'Narail-S'),
(482, 63, 'Lohagara'),
(483, 63, 'Kalia'),
(484, 64, 'Assasuni'),
(485, 64, 'Kalaroa'),
(486, 59, 'Satkhira'),
(487, 64, 'Kaliganj'),
(488, 64, 'Debhata'),
(489, 64, 'Shyamnagar'),
(490, 64, 'Tala'),
(491, 1, 'Dhaka'),
(492, 1, 'Gazipur'),
(493, 1, 'Manikgonj'),
(494, 1, 'Narsingdi'),
(495, 24, 'Chapaigonj'),
(496, 24, 'Rajshahi'),
(497, 59, 'Bagherhat'),
(498, 57, 'Kustia'),
(499, 57, 'Jhenaidah'),
(500, 57, 'Jessore'),
(501, 32, 'Saidpur'),
(502, 32, 'Thakurgaon'),
(503, 32, 'Kaligonj'),
(504, 32, 'Rangpur');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ak_program_name`
--
ALTER TABLE `ak_program_name`
  ADD PRIMARY KEY (`ak_prgm_id`);

--
-- Indexes for table `ak_school`
--
ALTER TABLE `ak_school`
  ADD PRIMARY KEY (`ak_schl_id`);

--
-- Indexes for table `ak_study_center`
--
ALTER TABLE `ak_study_center`
  ADD PRIMARY KEY (`ak_centr_id`);

--
-- Indexes for table `k_disttrict`
--
ALTER TABLE `k_disttrict`
  ADD PRIMARY KEY (`ak_dis_id`);

--
-- Indexes for table `upazilla`
--
ALTER TABLE `upazilla`
  ADD PRIMARY KEY (`up_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ak_program_name`
--
ALTER TABLE `ak_program_name`
  MODIFY `ak_prgm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `ak_school`
--
ALTER TABLE `ak_school`
  MODIFY `ak_schl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ak_study_center`
--
ALTER TABLE `ak_study_center`
  MODIFY `ak_centr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `k_disttrict`
--
ALTER TABLE `k_disttrict`
  MODIFY `ak_dis_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `upazilla`
--
ALTER TABLE `upazilla`
  MODIFY `up_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=505;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
