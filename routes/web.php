<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });
Route::get('/', 'StudyCenterController@studyCenter');
Route::post('/centerstore', 'StudyCenterController@storeStudyCenter');
Route::get('/getsrc', 'StudyCenterController@getSrc');
Route::get('/getprogram', 'StudyCenterController@getProgram');
Route::get('/src', 'rcSrcController@src');
Route::post('/storesrc', 'rcSrcController@storeSrc');
Route::get('/school', 'StudyCenterController@school');
Route::post('/schoolstore', 'StudyCenterController@schoolStore');
Route::get('/program', 'StudyCenterController@program');
Route::post('/programstore', 'StudyCenterController@programStore');
Route::get('/centers', 'StudyCenterController@centerList');
Route::get('/getcenterlist', 'StudyCenterController@getCenterList');
Route::get('/centeredit/{c_id}', 'StudyCenterController@centerEdit'); 
Route::post('/programupdate', 'StudyCenterController@programUpdate');
Route::get('/centerdelete/{c_id}', 'StudyCenterController@centerDelete'); 
