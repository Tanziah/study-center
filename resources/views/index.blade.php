<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
		<title></title>
		<link rel="icon" href="{{ asset('assets/images/logo/mbm.png') }}" type="image/png" sizes="32x32"/> 

		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<meta name="csrf-token" content="{{ csrf_token() }}">

	 <!-- plugins:css -->
	   <!--  <link rel="stylesheet" href="{{ asset('vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}"> -->
	    <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css') }}">
	    <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.addons.css') }}">
	    <!-- endinject -->
	    <!-- plugin css for this page -->
	    <!-- End plugin css for this page -->
	    <!-- inject:css -->
	    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
	    <!-- endinject -->
        <script src="{{ asset('js/jquery-3.3.1.js') }}"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>

	<body class="skin-1">
		
		@include('top_navbar')
		@include('menu')
		@yield('content')
		@include('footer')



	   <script src="{{ asset('js/off-canvas.js') }}"></script>
	   <script src="{{ asset('js/misc.js') }}"></script>

	   <!-- endinject -->
	   <!-- Custom js for this page-->
	   <!-- End custom js for this page-->
	</body>
</html>