<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
		<title></title>
		<link rel="icon" href="{{ asset('assets/images/logo/mbm.png') }}" type="image/png" sizes="32x32"/> 

		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<meta name="csrf-token" content="{{ csrf_token() }}">

	 <!-- plugins:css -->
	    <link rel="stylesheet" href="{{ asset('vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
	    <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css') }}">
	    <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.addons.css') }}">
	    <!-- endinject -->
	    <!-- plugin css for this page -->
	    <!-- End plugin css for this page -->
	    <!-- inject:css -->
	    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
	    <!-- endinject -->
	 
	</head>

	<body class="skin-1">
		
		@include('top_navbar')
		@include('menu')
		@include('study_center')
		@include('footer')

		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>

		<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

		<!-- Select2 Js -->
		<script src="{{ asset('assets/js/select2.min.js') }}"></script>

		<!-- Datatables Js -->
		<script src="{{ asset('assets/js/dataTables.min.js') }}"></script>

		<!-- Jquery Ui Js-->
		<script src="{{ asset('assets/js/jquery-ui.min.js') }}"></script>
		<script src="{{ asset('assets/js/jquery-ui.custom.min.js') }}"></script>
  
		<!-- Form Validation -->
		<script src="{{ asset('assets/js/jquery.form-validator.min.js') }}"></script> 

		<!-- Pie Chart -->
	    <script src="{{ asset('assets/js/jquery.easypiechart.min.js') }}"></script>
	    <script src="{{ asset('assets/js/jquery.sparkline.index.min.js') }}"></script>
	    <script src="{{ asset('assets/js/jquery.flot.min.js') }}"></script>
	    <script src="{{ asset('assets/js/jquery.flot.pie.min.js') }}"></script>
	    <script src="{{ asset('assets/js/jquery.flot.resize.min.js') }}"></script>

		<!-- Typeahead Js -->
		<script src="{{ asset('assets/js/bootstrap3-typeahead.min.js') }}"></script> 

		<!-- Tag Input -->
		<script src="{{ asset('assets/js/bootstrap-tag.min.js') }}"></script> 
		
		<!-- Tinymce Js by khirat -->
		<script src="{{ asset('assets/js/tinymce/tinymce.min.js') }}"></script> 

		<!-- Date Time Picker Js-->
		<script src="{{ asset('assets/js/moment.min.js') }}"></script>
		<script src="{{ asset('assets/js/bootstrap-datetimepicker.min.js') }}"></script>

		<!-- Ace Default -->
		<script src="{{ asset('assets/js/ace-elements.min.js') }}"></script>
		<script src="{{ asset('assets/js/ace.min.js') }}"></script>
		<!-- PDF and Excel Export -->
		
		<!-- Custom Js -->
		<script src="{{ asset('assets/js/custom.js') }}"></script> 


		<script src="{{ asset('vendors/js/vendor.bundle.base.js') }}"></script>
		<script src="{{ asset('vendors/js/vendor.bundle.addons.js') }}"></script>

	   <!-- endinject -->
	   <!-- Plugin js for this page-->
	   <!-- End plugin js for this page-->
	   <!-- inject:js -->

	   <script src="{{ asset('js/off-canvas.js') }}"></script>
	   <script src="{{ asset('js/misc.js') }}"></script>
	   <!-- endinject -->
	   <!-- Custom js for this page-->
	   <!-- End custom js for this page-->
	</body>
</html>