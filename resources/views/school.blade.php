@extends('index')
@section('content')
 <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            
            <div class="col-md-6 grid-margin stretch-card offset-3">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Add School</h4>
                 
                   <!-- Display Erro/Success Message -->
                     @include('message')
                   <form class="form-horizontal" role="form" method="post" action="{{ url('/schoolstore')  }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                
                    <div class="form-group">
                      <label for="school">School Name:<span style="color: red">&#42;</span> </label>
                       
                      <input type="text" name="school" class="form-control" value="" required="required"/>
                    </div>

                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                
                    <input type="reset" class="btn btn-light" name="Reset">
                  </form>
                </div>
              </div>
            </div>
           
        
          
          </div>
        </div>
        <!-- content-wrapper ends -->
     

@endsection