@extends('index')
@section('content')
 <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
              <div class="col-md-12 grid-margin stretch-card ">
              <div class="card">
                <div class="card-body">
                  <div class="form-group col-md-2 float-left">
                      <label for="exampleInputName1">RC :</label>
                 
                        <select id="rc" class="form-control" name="rc">
                          <option value="">Select RC</option>
                          @foreach($rcList as $rc)
                              <option value="{{$rc->ak_dis_id}}">{{$rc->dis_name}}</option>
                          @endforeach    
                      </select>
                  </div>
                  <div class="form-group col-md-2 float-left">
                      <label for="exampleInputName1">SRC :</label>
                        <select id="src" class="form-control" name="src">
                          <option value="">Select RC</option>
                        </select>
                  </div>
                  <div class="form-group col-md-2 float-left">
                      <label for="sc">SC :</label>
                 <input type="text" class="form-control" id="sc" placeholder="Enter SC" name="sc" value="">
                        <!-- <select id="sc"  class="form-control" name="sc">
                          <option value="">Select SC</option>
                          @foreach($scode as $code)
                              <option value="{{$code->   ak_study_center_code }}">{{$code->   ak_study_center_code }}</option>
                          @endforeach 
                      </select> -->
                  </div>
                   <div class="form-group col-md-2 float-left">
                      <label for="sc_name">School Name :</label>
                        <select id="school" class="form-control" name="sc_name">   
                             <option value="">Select School</option>
                          @foreach($schoolList as $scl)
                            <option value="{{$scl->ak_schl_id }}">{{$scl->ak_schl_name }}</option>
                          @endforeach               
                        </select>
                  </div>
                  <div class="form-group col-md-2 float-left">
                      <label for="pgrm_name">Program Name :</label>
                 
                        <select id="program" class="form-control" name="pgrm_name" >
                          <option value="">Select School</option>
                      </select>
                  </div>
                 
                  <div class="form-group col-md-2 float-left">
                    <br/>
                       <button type="submit" id="search"class="btn btn-primary btn-sm">Search</button>

                  </div>
                 
                </div>
              </div>
            </div>
            
            <div class="col-md-12 grid-margin stretch-card ">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title col-md-6 pull-left"> Program Study Center List</h4>
                  <button type="button" onClick="printMe('PrintArea')" class="showprint btn btn-warning btn-sm pull-right">
                                   <i class="fa fa-print"></i> 
                  </button>
                  <div id="PrintArea" class="table-responsive">
                    <table  class="table ">
                      <thead>
                        <tr>
                          <th>Study Center </th>
                          <th>SC</th>
                          <th>SRC</th>
                          <th>Program </th>
                          <th>School </th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="centerlist">
                       @foreach($programs as $prog)
                        <tr>
                          <td>{{$prog->ak_study_center}}</td>
                          <td>{{$prog->ak_study_center_code}}</td>
                          <td>{{$prog->upzilla_name}}</td>
                          <td>{{$prog->ak_prgm_name}}</td>
                          <td>{{$prog->ak_schl_name}}</td>
                       
                          <td>
                            <?php if($prog->ak_status==1)
                               { $status='<label class="col-sm-12 badge badge-success">Active</label>';
                               }
                               else
                               { $status='<label class="col-sm-12 badge badge-danger">Inactive</label>';
                               }
                                 echo $status;
                            ?>

                          </td>
                          <td> 
                            <a type="button" href="{{ url('/centeredit/'.$prog->ak_centr_id) }}"  style="font-size:18px" title="Update"><i class="fa fa-pencil"></i> </a> | 

                            <a href="{{ url('centerdelete/'.$prog->ak_centr_id) }}" onclick="return confirm('Are you sure you want to delete this Center?');" style="font-size:18px" title="Delete"><i class="ace-icon fa fa-trash bigger-120" style="color:#E00201;"></i></a></td>
                        </tr>
                        @endforeach  
                    
                       
                      </tbody>
                    </table>
                   
                  </div>
                </div>
                 <div class=col-md-6 pull-right"> {{ $programs->render() }} </div>
              </div>
            </div>
           
        
          
          </div>
        </div>
        <!-- content-wrapper ends -->

<script type="text/javascript">
  function printMe(divName)
{ 
    var myWindow=window.open('','','width=800,height=800');
    myWindow.document.write(document.getElementById(divName).innerHTML); 
    myWindow.document.close();
    myWindow.focus();
    myWindow.print();
    myWindow.close();
}

$(document).ready(function(){ 
  
/// RC List
  var action_element = $("#src");

   $('#rc').on("change", function(){ 

        // Action Element list
        $.ajax({
            url : "{{ url('/getsrc') }}",
            type: 'get',
            data: {rc_id : $(this).val()},
            success: function(data)
            {
                action_element.html(data);
            },

            error: function()
            {

                alert('failed...');
            }
        });

    });

// Program Based On School
var action_program = $("#program");

   $('#school').on("change", function(){ 

        // Action Element list
        $.ajax({
            url : "{{ url('/getprogram') }}",
            type: 'get',
            data: {sc_id : $(this).val()},
            success: function(data)
            {
                action_program.html(data);
            },
            error: function()
            {
                alert('failed...');
            }
        });

    });


/// Search Result


   $('#search').on("click", function(){ 
       var rc_val  = $("#rc").val();
       var src_val = $("#src").val();
       var sc_val  = $("#sc").val();
       var scl_val = $("#school").val();
       var prg_val = $("#program").val();
       var action_list= $("#centerlist");
  

        // Action Element list
        $.ajax({
            url : "{{ url('/getcenterlist') }}",
            type: 'get',
            data: {rc_id:rc_val, src:src_val, scode:sc_val, scl:scl_val,  prg:prg_val},
            success: function(data)
            {
                action_list.html(data);
            },
            error: function()
            {
                //console.log(data);
               alert('Center not Found.');

            }
        });

    });

/// 
});
</script>

@endsection