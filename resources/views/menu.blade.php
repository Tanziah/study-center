 <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
      <!--       <div class="nav-link">
              <div class="user-wrapper">
              
                <div class="text-wrapper">
                  <p class="profile-name btn btn-success btn-block"> Study Center</p>
                
                </div>
              </div>
          
            </div> -->
          </li>
          <li class="nav-item">
            <a class="nav-link" href=" {{ url('/') }}">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">New Study Center</span>
            </a>
          </li>
           <li class="nav-item">
            <a class="nav-link" href=" {{ url('/centers') }}">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">Search Center</span>
            </a>
          </li>
     
          <li class="nav-item">
            <a class="nav-link" href="{{ url('src') }}">
              <i class="menu-icon mdi mdi-backup-restore"></i>
              <span class="menu-title">SRC</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{ url('school') }}">
              <i class="menu-icon mdi mdi-backup-restore"></i>
              <span class="menu-title">School</span>
            </a>
          </li>
    
          <li class="nav-item">
            <a class="nav-link" href="{{ url('program') }}">
              <i class="menu-icon mdi mdi-backup-restore"></i>
              <span class="menu-title">Program</span>
            </a>
          </li>
        <!--   <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-content-copy"></i>
              <span class="menu-title">School</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="{{ url('school') }}">New School</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="">School List</a>
                </li>
              </ul>
            </div>
          </li>
    
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="" aria-expanded="false" >
              <i class="menu-icon mdi mdi-restart"></i>
              <span class="menu-title">Program</span>
              <i class="menu-arrow"></i><i class="fa fa-chevron-down"></i>
            </a>
            <div class="collapse" id="auth">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="{{ url('program') }}"> New Program</a>
                </li> -->
            <!--    
                <li class="nav-item">
                  <a class="nav-link" href=""> Program List </a>
                </li>
                
              </ul>
            </div>
          </li> -->
        </ul>
      </nav>