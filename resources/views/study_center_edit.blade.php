@extends('index')
@section('content')
 <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            
            <div class="col-md-6 grid-margin stretch-card offset-3">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Update Program Study Center</h4>
                  <!-- Display Erro/Success Message -->
                     @include('message')
                                    
                   <form class="form-horizontal" role="form" method="post" action="{{ url('/programupdate')  }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label for="exampleInputName1">RC:<span style="color: red">&#42;</span> </label>
                      
                      <select id="rc" class="form-control" name="rc" required="required">
                        
                        <option value="{{$stdcenter->ak_dis_id}}">{{$stdcenter->dis_name}}</option>
                          @foreach($rcList as $rc)
                        <option value="{{$rc->ak_dis_id}}">{{$rc->dis_name}}</option>
                          @endforeach    
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="src">SRC: <span style="color: red">&#42;</span> </label>
                        <select id="src" class="form-control" name="src" required="required">
                             <option value="{{$stdcenter->up_id}}">{{$stdcenter->upzilla_name}}</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="scl_name">School Name:<span style="color: red">&#42;</span> </label>
                      
                      <select id="school" class="form-control" name="scl_name" required="required">
                     
                         <option value="{{$stdcenter->ak_schl_id}}">{{$stdcenter->ak_schl_name}}</option>
                          @foreach($schoolList as $rc)
                              <option value="{{$rc->ak_schl_id}}">{{$rc->ak_schl_name}}</option>
                          @endforeach    
                      </select>
                    </div>
              
                    <div class="form-group">
                      <label for="pgrm_name">Program Name:<span style="color: red">&#42;</span> </label>
                      <select id="program" class="form-control" name="pgrm_name" required="required">
                         <option value="{{$stdcenter->ak_prgm_id}}">{{$stdcenter->ak_prgm_name}}</option>
                           
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="center_name">Study Center Name : <span style="color: red">&#42;</span> </label>
                      <input type="text" class="form-control" id="center_name" placeholder="Enter Study Center Name " name="center_name" value="{{$stdcenter->ak_study_center}}" required="required">
                    </div>
                    <div class="form-group">
                      <label for="center_name_bangla">Study Center Name Bangla:</label>
                      <input type="text" class="form-control" id="center_name_bangla" placeholder="Enter Study Center Name Bangla" name="center_name_bangla" value="{{$stdcenter->ak_study_center_bangla}}">
                    </div>
                    <div class="form-group">
                      <label for="std_center_code">Study Center Code:<span style="color: red">&#42;</span>  </label>
                      <input type="text" class="form-control" id="std_center_code" placeholder="Enter Study Center Code" name="std_center_code" value="{{$stdcenter->ak_study_center_code}}" required="required">
                    </div>
                    <div class="form-group">
                      <label for="address">Address:</label>
                      <textarea class="form-control" id="address" rows="2" name="address">{{$stdcenter->ak_address}}</textarea>
                    </div>
                    <div class="form-group">
                      <label for="scl_name">Status: <span style="color: red">&#42;</span> </label>
                      
                      <select id="status" class="form-control" name="status" required="required">
                        <option value="{{$stdcenter->ak_status}}">
                          <?php 
                             if($stdcenter->ak_status==1) echo "Active";
                             else echo"Inactive ";?>
                           </option>
                         <option value="1">Active</option>
                         <option value="0">Inactive </option> 
                      </select>
                    </div>
                    <br/>
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                
                    <input type="reset" class="btn btn-light" name="Reset">
                    <input type="hidden" value="{{$stdcenter->ak_centr_id}}" name="std_id">
                  </form>
                </div>
              </div>
            </div>
           
        
          
          </div>
        </div>
        <!-- content-wrapper ends -->

<script type="text/javascript">
$(document).ready(function(){ 
  
/// RC List
  var action_element = $("#src");

   $('#rc').on("change", function(){ 

        // Action Element list
        $.ajax({
            url : "{{ url('/getsrc') }}",
            type: 'get',
            data: {rc_id : $(this).val()},
            success: function(data)
            {
                action_element.html(data);
            },
            error: function()
            {
                alert('failed...');
            }
        });

    });

// Program Based On School
var action_program = $("#program");

   $('#school').on("change", function(){ 

        // Action Element list
        $.ajax({
            url : "{{ url('/getprogram') }}",
            type: 'get',
            data: {sc_id : $(this).val()},
            success: function(data)
            {
                action_program.html(data);
            },
            error: function()
            {
                alert('failed...');
            }
        });

    });

/// 
 
});
</script>
@endsection