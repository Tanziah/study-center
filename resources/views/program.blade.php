@extends('index')
@section('content')
 <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            
            <div class="col-md-6 grid-margin stretch-card offset-3">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Add Program </h4>
                  <!-- Display Erro/Success Message -->
                     @include('message')
                  
                   <form class="form-horizontal" role="form" method="post" action="{{ url('/programstore')  }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label for="exampleInputName1">School :</label>
                 
                        <select class="form-control" name="school" required="required">
                          @foreach($schoolList as $rc)
                              <option value="{{$rc->ak_schl_id}}">{{$rc->ak_schl_name}}</option>
                          @endforeach    
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="src">Program :</label>
                       
                      <input type="text" name="program" class="form-control  composition" value="" required="required"/>
                    </div>

                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                
                    <input type="reset" class="btn btn-light" name="Reset">
                  </form>
                </div>
              </div>
            </div>
           
        
          
          </div>
        </div>
        <!-- content-wrapper ends -->
     

@endsection