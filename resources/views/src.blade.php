@extends('index')
@section('content')
 <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            
            <div class="col-md-6 grid-margin stretch-card offset-3">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Add SRC</h4>
                  <!-- Display Erro/Success Message -->
                     @include('message')
                  
                   <form class="form-horizontal" role="form" method="post" action="{{ url('/storesrc')  }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label for="exampleInputName1">RC :</label>
                 
                        <select class="form-control" name="rc" required="required">
                          @foreach($rcList as $rc)
                              <option value="{{$rc->ak_dis_id}}">{{$rc->dis_name}}</option>
                          @endforeach    
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="src">SRC :</label>
                       
                      <input type="text" name="src" class="form-control  composition" value="" required="required" placeholder="Enter Upazila" />
                    </div>

                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                
                    <input type="reset" class="btn btn-light" name="Reset">
                  </form>
                </div>
              </div>
            </div>
           
        
          
          </div>
        </div>
        <!-- content-wrapper ends -->
     

@endsection